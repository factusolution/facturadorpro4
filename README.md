<img src="icon_pro3.jpg" width="320">

# **Facturador PRO4**

SE COMUNICA A NUESTRA COMUNIDAD DEL LANZAMIENTO DE LA NUEVA VERSIÓN FACTURADOR PRO5, EL CUAL ESTARÁ AL ALCANCE DE TODOS NUESTROS CLIENTES CON UN DESCUENTO DEL 60% DE SU PRECIO REGULAR, ES DECIR A SOLO S/350.00
CABE MENCIONAR QUE LA VERSIÓN PRO4 SEGUIRÁ RECIBIENDO ACTUALIZACIONES PERIÓDICAS Y SE PODRÁ ACCEDER A ELLAS DE LA MISMA MANERA QUE SE VIENE EFECTUANDO HASTA EL MOMENTO

**PARA ACCEDER A DEMO DE LA VERSION PRO5 PUEDE INGRESAR A**:
https://demo.rrfact.com
Usuario y contraseña: demo@rrfact.com

### Manuales de Instalación

[Instalación PC Windows - Linux](https://docs.google.com/document/d/1NGDxJwrqL6pneBad1bwB4-lzb7vmsgwFTr6oxB4kodw/edit?usp=sharing "Clic")
<br>
[Servidor WEB- Instancia VPS Docker - Linux](https://docs.google.com/document/d/1rguv6CGoCNQEalz5fwOcDRQcHCO3ebkSSvy4eBAreI8/edit?usp=sharing "Clic")
<br>
[Valet - Linux](https://docs.google.com/document/d/1B0sqxV0bkXaD6iBpQuh-oPiSjBYG_oAHeNiUpyMQ3Nw/edit?usp=sharing "Clic")
<br>
[Linux - gestión externa de SSL](https://docs.google.com/document/d/1qJZerdJh8W0l-W21DiXtOTSEk3A25OZG03x7zHmpOrI/edit?usp=sharing "Clic")


### APP Facturación Electrónica

[Descarga de APP para celular]( https://drive.google.com/file/d/1GtvnYfI992DUDKjp3A-HpXegoY7JtXK5/view?usp=sharing "Clic")
<br>
[Manual de Uso de APP para celular](https://docs.google.com/document/d/1R44Gvh6XzDDpIarIQOqpEY6UXjgFhA6CWDjvO4Lql7I/edit?usp=sharing"Clic")
<br>


### Manuales de actualización

* Docker - Comandos manuales

[Con Docker + Gitlab](https://docs.google.com/document/d/16h7PTiaK4AQqSqljUuAKcJEM5WVeYOqaFytFfRWmUPM/edit?usp=sharing "Clic")
<br>

[SSL + Docker](https://docs.google.com/document/d/1Gau1fH7zB9dcQa9WiJPREdTjYTUU4JoK6X3V8GDHBFQ/edit?usp=sharing "Clic")<br>


## Soporte

* Se recomienda programar backup frecuentes en las instancias para salvaguardar la documentación importante de las plataformas de facturación; para ello se recomienda EXPORTAR todos los sistemas (clientes - tenancy) desde la bases de datos además de ello descargar la carpeta STORAGE desde la raiz del sistema accediendo mediante FTP.



## R&R SOLUCIONES CONTABLES

factusolution@gmail.com<br>
wsapp: 938 169 434<br>
